# Dokumentation Migrationsprojekt M158

[TOC]

## Auftrag
Wir haben uns dazu entschlossen, eine Migration einer WordPress-Seite durchzuführen, wie im [Modul-Repo beschrieben](https://gitlab.com/ch-tbz-it/Stud/m158/-/tree/main/02_Projekt%20Migration/WordPress-Web-App-Migration). Allerdings haben wir uns dazu entschieden, nicht eine Proxmox-VM, welche von der TBZ zur Verfügung gestellt wurde, zu nutzen. Wir mieten eine kleine VPS-Instanz zu mieten und die Webseite darauf zu migrieren. Diese ist erreichbar unter: [wp-toor.m158.etlas.one](https://wp-toor.m158.etlas.one).

## Ziele (teils definiert durch Herr Aeschlimann)

*   [x] Ein Datenbankserver wurde auf Linux erfolgreich installiert und konfiguriert
*   [x] Die Datenbank von der Quellinstallation wurde migriert
*   [x] Es wurden neuen Datenbankbenutzer angelegt und entsprechende Berechtigungen vergeben
*   [X] Es wurde ein FTP-Dienst für das transferieren der Dateien eingerichtet
*   [X] Ein FTP-User welcher nur das Root-Verzeichnis der Website sieht wurde eingerichtet
*   [X] Es wurde ein Webserver eingerichtet
*   [X] Webserver läuft unter einem virtuellen Host Ihrer Wahl
*   [X] HTTP wird auf HTTPS umgeleitet inkl. TLS-Zertifikat
*   [X] Es wurde PHP installiert und aktiviert
*   [X] Es wird min. PHP Version 8.2 eingesetzt
*   [X] Die Website sieht optisch identisch wie die Quellapplikation aus und zeigt keine Fehlermeldungen an
*   [x] Die Seite [https://wp-toor.m158.etlas.one/wp-admin/site-health.php](https://wp-toor.m158.etlas.one/wp-admin/site-health.php) zeigt keine Fehler
*   [x] Die Seite [https://wp-toor.m158.etlas.one/wp-admin/admin.php?page=et\_support\_center\_divi](https://wp-toor.m158.etlas.one/wp-admin/admin.php?page=et_support_center_divi) zeigt keine Fehler auf
*   [X] Ein Datei- und Datenbank-Backup ist Serverseitig eingerichtet
*   [x] Website ist auch erreichbar über IPv6
*   [X] Website ist mit TLS Zertifikat von Let's Encrypt ausgestattet
*   [X] Website ist über FQDN erreichbar
## Umsetzung

### Ein Datenbankserver wurde auf Linux erfolgreich installiert und konfiguriert

Zusammen mit dem apache2 sowie einigen PHP Modulen haben wir via apt mysql-server (mariadb) installiert.
```shell
sudo apt install apache2 ghostscript libapache2-mod-php mysql-server php php-bcmath php-curl php-imagick php-intl php-json php-mbstring php-mysql php-xml php-zip
```
Folgendermassen haben wir dann die Datenbank konfiguriert (die Angaben konnten wir der Datei "wp-config.php", welche im Export des Legacy-Servers enthalten war):

```shell
sudo mysql -u root
```

```sql
CREATE DATABASE wp_m158_db;
CREATE USER wp_m158_user@localhost IDENTIFIED BY '0Cc6j_6otBljfdfG';
GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,ALTER
    -> ON wp_m158_db.*
    -> TO wp_m158_user@localhost; 
FLUSH PRIVILEGES;
```
```shell
systemctl restart mysql
```
Diese Angaben müssen später beim Konfigurieren der WordPress-Installation über das Webinterface angegeben werden.

### Die Datenbank von der Quellinstallation wurde migriert

Mittels dem Plugin [All-in-One WP Migration](https://servmask.com/) konnte auf dem [Legacy Server](https://m158.geekz.ch/) die gesamte Datenbank sowie alle Wordbress Konfigurationen und Einstellungen exportiert werden. Via dem selben Plugin konnten wir auf dem Zielserver, nachdem eine Standard-Installation von Wordpress erfolgt ist, die Daten einlesen. Im Hindergrund sorgte das Plugin dafür, dass die Domäne sämtlicher Links entsprechend unserer Domain angepasst wurden.

### Es wurden neuen Datenbankbenutzer angelegt und entsprechende Berechtigungen vergeben
siehe [Ein Datenbankserver wurde auf Linux erfolgreich installiert und konfiguriert](#L31)
### Es wurde ein FTP-Dienst für das transferieren der Dateien eingerichtet

Wir haben das vsftpd apt Packet installiert:

```shell
apt install vsftp
```

Dann die Config folgendermassen angepasst:
```shell
# Dass der FTP User nur auf sein home Verzeichnis Zugriff hat.
chroot_local_user=YES
# Dass der FTP User auch schreib Rechte in diesem Verzeichnis hat.
allow_writable_chroot=YES
# Zur Sicherheit auf ein TLS Zertifikat verweisen, dass eine FTPs Verbindung verwendet wird. Und SSL aktiviert.
rsa_cert_file=/etc/letsencrypt/live/wp-toor.m158.etlas.one/fullchain.pem
rsa_private_key_file=/etc/letsencrypt/live/wp-toor.m158.etlas.one/privkey.pem
ssl_enable=YES 
```

Dass der FTP user auf den DocumentRoot des Webservers Zugriff auf das Verzeichnis hat, wurde dieser der `www-data` Gruppe hinzugefügt. Ebenfalls haben wir dann das Home Verzeichnis des FTP Users auf den Ordner der Webseite gelegt, dass er, mithilfe der chroot einstellungen nur auf dieses Verzeichnis Zugriff hat.

### Ein FTP-User welcher nur das Root-Verzeichnis der Website sieht wurde eingerichtet

Siehe: [Es wurde ein FTP-Dienst für das transferieren der Dateien eingerichtet](#es-wurde-ein-ftp-dienst-für-das-transferieren-der-dateien-eingerichtet)

![FTP-User Zugriff](/doc/media/ftpuser.png)

### Es wurde ein Webserver eingerichtet

Wir haben uns beim Webserver für den Apache2 entschieden. Wie bereites unter Punkt: [Ein Datenbankserver wurde auf Linux erfolgreich installiert und konfiguriert](#ein-datenbankserver-wurde-auf-linux-erfolgreich-installiert-und-konfiguriert) diverse Packete installiert unter anderem Apache2.

Mittels folgenden Befehlen erstellen wir einen Orfdner /srv/www und legen die Standard-Wordpress-Installation in dieser Location ab:

```bash
sudo mkdir -p /srv/www
sudo chown www-data: /srv/www
curl https://wordpress.org/latest.tar.gz | sudo -u www-data tar zx -C /srv/www
```


Wir haben eine neue vhost-File im Ordner `/etc/apache2/sites-available/` erstellt (wordpress.conf), in dieser haben wir unsere Domain definiert, um später via HTTP auf Wordpress zugreifen zu können. Als DocumentRoot haben wir /srv/www/wordpress definiert. In diesem Ordner werden wir Wordpress installieren.

```
<VirtualHost *:80>
    ServerName wp-toor.m158.etlas.one
    ServerAlias ipv6.wp-toor.m158.etlas.one
    DocumentRoot /srv/www/wordpress
    <Directory /srv/www/wordpress>
        Options FollowSymLinks
        AllowOverride Limit Options FileInfo
        DirectoryIndex index.php
        Require all granted
    </Directory>
    <Directory /srv/www/wordpress/wp-content>
        Options FollowSymLinks
        Require all granted
    </Directory>
</VirtualHost>
```


Mittels `sudo a2ensite wordpress` wurde die Webseite aktiviert.

Nun konnte mit Aufruf unserer Domain, mittels dem Wordpress Wizard eine Standard-Installation durchgeführt werden. Da später durch unser Plugin ([Die Datenbank von der Quellinstallation wurde migriert](#die-datenbank-von-der-quellinstallation-wurde-migriert)) sowieso unsere gesamte Wordpress Seite durch den Export der Legacy Seite ersetzt wird, spielt hier die genaue Konfiguratoion abgesehen von den Datenbank-Credentials keine grosse Rolle.

Um den Upload der Export-datei von unserem Legacy Server via dem erwähnten Plugin auf unseren neuen Server zu ermöglichen, mussten noch einige Konfigurationen hinsichtlich maximale Upload-Grösse angepasst werden. Alsbald dies erfolgt ist, kann die Sicherungsdatei vom Legacy Server via demselben Plugin auf unseren neuen Server hochgeladen werden und das Plugin erledigt sämtliche Migrationsarbeiten.

In der .htaccess File:

```
php_value upload_max_filesize 512M                                                                                      php_value post_max_size 512M                                                                                            php_value memory_limit 512M                                                                                             php_value max_execution_time 300                                                                                        php_value max_input_time 300  
```

In der wp-config.php File:

```
@ini_set( 'upload_max_filesize' , '512M' );                                                                             @ini_set( 'post_max_size', '512M');                                                                                     @ini_set( 'memory_limit', '512M' );                                                                                     @ini_set( 'max_execution_time', '300' );                                                                                @ini_set( 'max_input_time', '300' );  

```

![Import the Export](/doc/media/import-theexporttonewserver.png)
![]()

### Webserver läuft unter einem virtuellen Host Ihrer Wahl
Unter Apache2 wird immer auf "virtuelle Hosts" bzw. "vhosts" zurückgegriffen. Im Ordner `/etc/apache2/sites-available/` legen wir .conf Dateien ab, in welchen wir auf unserem Webserver mehrere Domains und jeweils eigene Root-Verzeichnisse definieren können. Auf unserem Webserver finden sich aktuell zwei vhost Dateien, eine für unsere Wordpress Seite unter Port 80 via HTTP und eine für diesselbe Wordpress Seite unter derselben Domain aber unter Port 443 und somit erreichbar via HTTPS.

### HTTP wird auf HTTPS umgeleitet mit einem selbst signiertem Zertifikat
Anstelle eines selbst signierten Zertifikats wurde ein Zertifikat verwendet, welches automatisiert von Let's Encrypt ausgestellt wurde. (siehe Punkt [Website ist mit TLS Zertifikat von Let's Encrypt ausgestattet](#website-ist-mit-tls-zertifikat-von-lets-encrypt-ausgestattet))

In der Virtual Host File für Wordpress unter Apache2 (/etc/apache2/sites-available/wordpress.conf) legen wir die Domains fest, unter welcher unsere Wordpress Seite erreichbar sein soll. Als Alias auch unsere ipv6-only Domain. Certbot wird später nach der Erstellung des TLS-Zertifikats automatisch einen Redirect von HTTP auf HTTPS einrichten. Manuell könnte dies selbstverständlich auch vorgenommen werden (ebenfalls in dieser wordpress.conf Datei).


![Virtiualhost](/doc/media/virtualhost-port80-domains.png)
![Redirect](/doc/media/redirecvtrule.png)

![http-weiterleitung](/doc/media/httpweiterleitung.png)

### Es wurde PHP installiert und aktiviert
Wir haben PHP 8.2 aus den Standard-Ubuntu-Packetquellen installiert und entsporechend unter Apache2 auch enabled:

```shell
sudo apt install php8.2 
sudo apt install php8.2-mysql php8.2-bcmath php8.2-curl php8.2-imagick php8.2-dom php8.2-zip php8.2-intl
a2dismod php8.1
sudo a2enmod php8.2

```
### Es wird min. PHP Version 8.2 eingesetzt
[siehe Abschnitt zur PHP Installation](#es-wird-min-php-version-82-eingesetzt)

![Site-Healthg](/doc/media/php8-2-all.png)

### Die Website sieht optisch identisch wie die Quellapplikation aus und zeigt keine Fehlermeldungen an

![DiviAi](doc/media/diviai.png)

### Die Seite [https://neue-url/wp-admin/site-health.php](https://neue-url/wp-admin/site-health.php) zeigt keine Fehler

![Bild Site-Health](/doc/media/webseitezustand.png)

### Die Seite [https://neue-url/wp-admin/admin.php?page=et\_support\_center\_divi](https://neue-url/wp-admin/admin.php?page=et_support_center_divi) zeigt keine Fehler auf
![Divi Helper Overview](/doc/media/divihelpoverview.png)
![Divi Helper Detail](/doc/media/divihelpdetail.png)

### Ein Datei- und Datenbank-Backup ist Serverseitig eingerichtet
Das Plugin (BackWPup), welches bereits auf de3m Legacy-Server installiert war, haben wir ebenfalls auf unserer Wordpress Seite installiert und einen Auftrag eingerichtet, welcher regelmässig ausgeführt wird und somit auch in regelmässigen Abständen ein Backup der gesmaten Webseite und Datenbank vornimmt.

AKtuell wird dieses Backup auf dem Server selbst abgelegt, es wäre natürlich für die Zukunft wünschenswert, diese Sichereungsdatei jeweils direkt an einem anderen Ort abzulegen.

![Backup](/doc/media/backup.png)
![Backup Zielordner](/doc/media/backup-zielordner.png)

### Website ist auch erreichbar über IPv6
Es wurde für die Subdomain (ipv6.wp-toor.m158.etlas.one) ein AAAA Record mit der IPv6-Adresse unseres VPS erstellt. Unter derselben SUbdomain ist jedoch kein A Record und folglich auch keine Möglichkeit die Webseite über diese eine Domain via IPv4 zu erreichen.

Wird auf die Hauptseite via IPv6 zugegriffen, wird die Anfrage auf IPv4 (und folglich die Stammdomäne) umgeleitet. Wird jedoch auf eine bestimmte Seite (also mit einer gesamten URL inkl. Datei) zugegriffen, erfolgt keine Umleitung und die Webseite kann ohne weiteres via IPv6 betrachtet werden.

![Bild - Ipv6 dig AAAA](/doc/media/digaaaipv6.png)
![Bild - Redirect](/doc/media/curlmovedipv6.png)
![Bild - WP-admin via IPv6](/doc/media/wp-adminviaupi6.png)

### Website ist mit TLS Zertifikat von Let's Encrypt ausgestattet
Anstelle eines selbst signierten Zertifikats wurde ein Zertifikat verwendet, welches automatisiert von Let's Encrypt ausgestellt wurde. Via dem Packet "Certbot" der Electronic Frontier Foundation (EFF) kann automatisiert mit wenigen Befehlen ein solches Zertifikat generiert werden und wird auch automatisiert nach 60 bzw. 90 Tagen durch certbot erneuert. Durch ein solches gültiges Zertifikat wird auch verhindert, dass beim Aufrufen der Webseite über HTTPS eine Warnung vor einem möglichen Man-in-the-middle im Browser erscheint.

```shell
sudo apt install certbot python3-certbot-apache
sudo certbot --apache
```

Certbot stellt einige Fragen, die zum Erstellen eines TLS Zertifikats notwendig sind und generiert danach den private Key sowie den Public Key und legt diese in den Ordner `/etc/letsencrypt/live/wp-toor.m158.etlas.one`.

Wir sehen im Ordner `/etc/apache2/sites-available`, dass Certbot eine neue Datei (wordpress-le-ssl.conf) abgelegt hat, in welcher ein neuer VHOST unter Port 443 (HTTPS) definiert wird.

![certbot](/doc/media/vhosthttps.png)
### Website ist über FQDN erreichbar
![Dig A](/doc/media/dig-a.png)
