# Backup / Get Data from Legacy Infrastructure

- Einloggen via https://m158.geekz.ch/wp-admin in Admin-Oberfläche von WordPress
- Backup anfertigen
    - https://blog.hubspot.com/website/how-to-migrate-wordpress-site
    - https://wordpress.com/go/tutorials/migrate-wordpress-website/
    - Plugin "BackWPup" nutzen
        - Neuer Backup-Auftrag wurde erstellt und ausgeführt -> ZIP-Datei heruntergeladen
        - Protokoll des Backup-Auftrags prüfen um sicherzustellen, dass keine Fehler aufgetreten sind